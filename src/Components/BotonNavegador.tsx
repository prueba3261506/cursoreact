import React from 'react'
import { useNavigate } from 'react-router-dom'


interface Props{
    name:string,
    ruta:string,
    magenDerecha?:boolean;
    style?:React.CSSProperties;
  }

export const BotonNavegador = ({name, ruta, magenDerecha=true, style}:Props) => {

    const navigate=useNavigate()

    const navegarADestino=()=>{
        navigate(ruta)
    }

  return (
    <button className='btn btn-primary col-1'onClick={navegarADestino} style={{...style,marginRight:magenDerecha ? 10:undefined}}>{name}</button>
    
  )
}
