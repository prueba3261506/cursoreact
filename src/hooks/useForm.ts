import { useState } from "react"

export const useForm =<T extends Object>(estadoInicial: T) => {
    const [formulario, setFormulario]=useState(estadoInicial)
    
    const onChange =<K extends Object>(campo:keyof T, valor:K)=>{

        setFormulario(
            {
                ...formulario, [campo]:valor
            }
        )
    }

    const setNewFormValue=(nuevoForm:T)=>{
        setFormulario(nuevoForm)
    }
    
    const reset = () => {
        setFormulario(estadoInicial)
    }

    return{

        ...formulario, formulario,onChange, reset,setNewFormValue,
    }

}