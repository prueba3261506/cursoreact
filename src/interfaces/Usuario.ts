export interface Usuario{
    id: number,
    username:string,
    nombre:string,
    apellido:string,
    cedula: string
}