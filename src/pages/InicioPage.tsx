import { BotonNavegador } from "../Components/BotonNavegador"


export const InicioPage = () => {

  return (
    <>
        <h1>Inicio</h1>
        <hr/>
        <BotonNavegador name="Productos" ruta="/productos" magenDerecha />
        <BotonNavegador name="Usuarios" ruta="/usuarios"/> 
        <BotonNavegador name="Categoria" ruta="/categorias"/> 
        

    </>
    
  )
}


// otra forma de aplicar estilos
const style={
  buttonStyle:{
    marginRight:10
  }

}
