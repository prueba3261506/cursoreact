import { useState } from "react"
import { BotonVolver } from "../../Components/BotonVolver"
import { useForm } from "../../hooks/useForm"
import { Categoria } from "../../interfaces/Categoria"
import { BotonNavegador } from "../../Components/BotonNavegador"
import { useNavigate } from "react-router-dom"

const estadoInicial ={
  nombre:"", descripcion:""
}

export const CategoriaPage = () => {

  const [categorias, setCategorias]=useState<Categoria[]>([])
  
  const {nombre,descripcion,onChange,reset, setNewFormValue}=useForm(estadoInicial)

  const [categoriaSeleccionada, setCategoriaSeleccionada]=useState<number|undefined>(undefined)

  const navigate=useNavigate()

  const eliminarCategoria=(idCategoria:number)=>{
    const nuevaLista=categorias.filter(categoria=>idCategoria !== categoria.id)
    setCategorias(nuevaLista)
  }


  const onEditar=()=>{

    if(categoriaSeleccionada){
      const nuevaLista= categorias.filter(categoria=> categoriaSeleccionada!== categoria.id)
    
     const categoria= {
      id: categoriaSeleccionada,
      nombre,
      descripcion,
    }

    nuevaLista.push(categoria);
    nuevaLista.sort((cat1,cat2)=>cat1.id -cat2.id)

    setCategorias(nuevaLista)
    
    }
    cerrarModoEdicion()
  }

  const onCancelar=()=>{
    cerrarModoEdicion()
  }

  const cerrarModoEdicion=()=>{
    setCategoriaSeleccionada(undefined)
    reset()
  }

  const abrirModoEdicion=(categoria:Categoria)=>{
    setCategoriaSeleccionada(categoria.id)
    setNewFormValue({
      nombre:categoria.nombre,
      descripcion: categoria.descripcion
    })
  }

  const onSelect=(categoria:Categoria)=>{

    const params=`?nombre=${categoria.nombre}&descripcion=${categoria.descripcion}`
    navigate(`/Categorias/${categoria.id}`+params)
 
  }



  const agregarCategoria=(nombre:string, descripcion:string)=>{
    let idMaximo=0;
    categorias.forEach((categoria)=>{
      if(categoria.id>idMaximo){
        idMaximo=categoria.id
      }

    })

    const nuevaCategoria=
    {
      id:idMaximo+1,
      nombre,
      descripcion
    }

    setCategorias([...categorias,nuevaCategoria])
  }

  const onAgregarClicked=()=>{

    if(nombre.length===0 || descripcion.length===0){
      alert("Todos los campos son requeridos")
      return
    }
    agregarCategoria(nombre, descripcion)
    reset()
  }

  return (
    <>
      <div className="container mt-5">
        <h2 className="mb-4">Categorìas</h2>
        <hr/>
        <div className="mb-3">
          <label htmlFor="nombre" className="form-label">Nombre</label>
          <input className="form-control form-control-xs" type="text" name="precio" placeholder="Ingrese el Nombre"
            value={nombre} onChange={(event) =>onChange("nombre",event.target.value)}/>
        </div>

        <div className="mb-3">
          <label htmlFor="nombre" className="form-label">Descripciòn</label>
          <input className="form-control form-control-xs" type="text" name="descripcion" placeholder="Ingrese la Descripciòn"
            value={descripcion} onChange={(event) =>onChange("descripcion",event.target.value)}/>
        </div>
                  
          { categoriaSeleccionada?<>
                  <button className="btn btn-primary col-2" onClick={()=> onEditar() } style={{marginLeft:10}}>
                    Editar
                  </button>

                  <button className="btn btn-outline-danger col-2" onClick={()=> onCancelar() } style={{marginLeft:10}}>
                  Cancelar
                </button>
                </>
                :<button className="btn btn-success col-3" onClick={()=> onAgregarClicked() } style={{marginLeft:10}}>
                  Agregar Categorìa
                </button>
                }


        <div className="table-responsive" style={{ marginTop:20, height: '300px', overflowY: 'scroll' }}>
          <table className="table table-striped table-hover">
            <thead>
               <tr className="table-active">
                  <th scope="col" >ID</th>
                  <th className="col-1">Nombre</th>
                  <th className="col-1">Descripción</th>
                  <th scope="col" >Acciones</th>
                </tr>
              </thead>
              <tbody>
                {
                  categorias.map(categoria=> <tr key={categoria.id} className="user-select-none" style={{cursor:`pointer`}}
                  onClick={()=>onSelect(categoria)}>

                  <th>{categoria.id}</th>
                  <td>{categoria.nombre}</td>
                  <td>{categoria.descripcion}</td>
                  <td>
                      <button className="btn btn-primary"
                        onClick={(event)=>{event.stopPropagation();
                        abrirModoEdicion(categoria)}}>
                        Editar
                      </button>
                            
                      <button className="btn btn-danger"
                        onClick={()=>eliminarCategoria(categoria.id)}>
                        Eliminar
                      </button>
                    </td>
                    </tr>)
                      }
                </tbody>
              </table>
                Total de categorias:<b>{categorias.length}</b>
          </div>
                        <BotonVolver></BotonVolver>
                        <BotonNavegador name="Inicio" ruta="/inicio"/>
                </div>
          </>
      
    )
  }
  