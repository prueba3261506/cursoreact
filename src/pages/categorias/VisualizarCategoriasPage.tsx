import { useEffect, useState } from "react"
import { Categoria } from "../../interfaces/Categoria"
import { useLocation, useParams } from "react-router-dom"
import { BotonVolver } from "../../Components/BotonVolver"

export const VisualizarCategoriasPage = () => {
    const {id}=useParams()
    const {search}=useLocation()
    const [categoria, setCategoria]=useState<Categoria>()

    useEffect( () => {
        const queryParams=new URLSearchParams(search)
        const nombre=queryParams.get('nombre')??'';
        const descripcion=queryParams.get('descripcion')??'';
        
        setCategoria({
            id:+(id??0), descripcion, nombre:nombre
        })

    },[search, id])


    return(
        <div>
            <h1> Visualizar Categoria: {id}</h1>
            <hr/>

            <table className="table table-bordered border-dark-subtle">
                <tbody>
                    <tr>
                        <th className="table-active">ID</th><td>{id}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Nombre</th><td>{categoria?.nombre}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Descripcion</th><td>{categoria?.descripcion}</td>
                    </tr>
                </tbody>
            </table>
            <BotonVolver/>
        </div>
  

     );
    
}