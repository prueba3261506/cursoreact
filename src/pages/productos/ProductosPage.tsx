import { useForm } from "../../hooks/useForm"
import { useState } from "react"
import { BotonVolver } from "../../Components/BotonVolver"
import { Producto } from "../../interfaces/Producto"
import { BotonNavegador } from "../../Components/BotonNavegador"
import { Navigate, useNavigate } from "react-router-dom"


const estadoInicial={
  descripcion: "", precio:"", fecha_de_ingreso:""
}



export const ProductosPage = () => {

  const [productos, setProductos]= useState<Producto[]>([])

  const {descripcion, precio,fecha_de_ingreso,onChange,reset,setNewFormValue}=useForm(estadoInicial)

  const [productoSeleccionado, setProductoSeleccionado]=useState<number|undefined>(undefined)

  const navigate=useNavigate()
  
  const eliminarProducto= (idProducto:number) =>{
    const nuevaLista= productos.filter(producto=> idProducto!== producto.id)
    setProductos(nuevaLista)
    alert(`Producto eliminado`)
  }


  const onEditar=()=>{

    if(productoSeleccionado){
      const nuevaLista= productos.filter(producto=> productoSeleccionado!== producto.id)
    
     const producto= {
      id: productoSeleccionado,
      descripcion,
      precio:+precio,
      fecha_de_ingreso

    }

    nuevaLista.push(producto);
    nuevaLista.sort((prod1,prod2)=>prod1.id -prod2.id)

    setProductos(nuevaLista)
    
    }
    cerrarModoEdicion()
  }

  const onCancelar=()=>{
    cerrarModoEdicion()
  }

  const cerrarModoEdicion=()=>{
    setProductoSeleccionado(undefined)
    reset()
  }

  const abrirModoEdicion=(producto:Producto)=>{
    setProductoSeleccionado(producto.id)
    setNewFormValue({
      descripcion:producto.descripcion,
      precio: producto.precio.toString(),
      fecha_de_ingreso: producto.fecha_de_ingreso
    })
  }

 
  const onSelect=(producto:Producto)=>{
   /* setProductoSeleccionado(producto.id)
    setNewFormValue({
      descripcion:producto.descripcion,
      precio: producto.precio.toString(),
      fecha_de_ingreso: producto.fecha_de_ingreso
    })*/
    const params=`?descripcion=${producto.descripcion}&precio=${producto.precio}&fecha_ingreso=${producto.fecha_de_ingreso}`
    navigate(`/productos/${producto.id}`+params)

  }

  const agregarProducto=(descripcion:string,precio:number, fecha_de_ingreso:string)=>{
    let idMaximo=0;
    productos.forEach((producto)=>{
      if(producto.id>idMaximo){
        idMaximo=producto.id
      }

    })
    const nuevoProducto=
    {
      id:idMaximo+1,
      descripcion,
      precio,
      fecha_de_ingreso
    }

    setProductos([...productos,nuevoProducto])

  }

  const onAgregarClicked=()=>{

    if(descripcion.length===0 || precio.length===0 || fecha_de_ingreso.length===0){
      alert("Ingrese todos los campo")
      return
    }

    agregarProducto(descripcion,+precio, fecha_de_ingreso)
    reset()
    //setDescripcion("")
    //setPrecio("")
  }

    return (
      <div className="container mt-5">
              <h2 className="mb-4">Productos</h2>
              <hr/>
                <div className="mb-3">
                  <label htmlFor="nombre" className="form-label">
                    Precio
                  </label>
                  <input className="form-control form-control-xs" type="number" name="precio" placeholder="Ingrese el Precio"
                  value={precio} onChange={(event) =>onChange("precio",event.target.value)}/>
                </div>

                <div className="mb-3">
                  <label htmlFor="nombre" className="form-label">
                    Descripciòn
                  </label>
                  <input className="form-control form-control-xs" type="text" name="descripcion" placeholder="Ingrese la Descripciòn"
                  value={descripcion} onChange={(event) =>onChange("descripcion",event.target.value)}/>
                </div>

                <div className="mb-3">
                  <label htmlFor="nombre" className="form-label">
                    Fecha
                  </label>
                  <input className="form-control form-control-xs" type="date" name="fecha_de_ingreso"
                  value={fecha_de_ingreso.split("/").reverse().join("-")} onChange={(event) =>{
                    const date =new  Date (event.target.value)
                    onChange("fecha_de_ingreso",`${date.getDate().toString().padStart(2,'0')}/${(date.getMonth()+1).toString().padStart(2,'0')}/${date.getUTCFullYear()}`)}}/>
                </div>

                { productoSeleccionado?<>
                  <button className="btn btn-primary col-2" onClick={()=> onEditar() } style={{marginLeft:10}}>
                    Editar
                  </button>

                  <button className="btn btn-outline-danger col-2" onClick={()=> onCancelar() } style={{marginLeft:10}}>
                  Cancelar
                </button>
                </>
                :<button className="btn btn-success col-3" onClick={()=> onAgregarClicked() } style={{marginLeft:10}}>
                  Agregar productos
                </button>
                }
                
                
                <div className="table-responsive" style={{ marginTop:20, height: '300px', overflowY: 'scroll' }}>
                    <table className="table table-striped table-hover">
                      <thead>
                        <tr className="table-active">
                        <th scope="col" >ID</th>
                          <th className="col-1">Precio</th>
                          <th className="col-1">Descripción</th>
                          <th className="col-1">Fecha de Ingreso</th>
                          <th scope="col" >Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      {
                        productos.map(producto=> <tr key={producto.id} className="user-select-none" style={{cursor:`pointer`}}
                        onClick={()=>onSelect(producto)}
                        >
                          <th>{producto.id}</th>
                          <td>{producto.descripcion}Gs.</td>
                          <td>{producto.precio}</td>
                          <td>{producto.fecha_de_ingreso}</td>
                          <td>
                            <button className="btn btn-primary"
                              onClick={(event)=>{event.stopPropagation();
                              abrirModoEdicion(producto)}}>
                                Editar
                            </button>
                          
                            <button className="btn btn-danger"
                              onClick={()=>eliminarProducto(producto.id)}>
                                Eliminar
                            </button>
                          </td>
                        </tr>)
                      }
                        
                      </tbody>
                    </table>
                    Total de productos:<b>{productos.length}</b>
                  </div>
                  
                    <BotonVolver></BotonVolver>
                    <BotonNavegador name="Inicio" ruta="/inicio"/>
            </div>
    )}


    const formatearFecha=(fecha:string): string =>{

      return fecha.split('-').reverse().join('/')
    }