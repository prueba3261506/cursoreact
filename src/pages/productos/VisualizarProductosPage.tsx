import { useLocation, useParams } from "react-router-dom";
import { BotonVolver } from "../../Components/BotonVolver";
import { useEffect, useState } from "react";
import { Producto } from "../../interfaces/Producto";


export const VisualizarProductosPage = () => {
    const {id}=useParams()
    const {search}=useLocation()
    const [producto, setProducto]=useState<Producto>()

    useEffect( () => {
        const queryParams=new URLSearchParams(search)
        const descripcion=queryParams.get('descripcion')??'';
        const precio=queryParams.get('precio')??'';
        const fecha_ingreso= queryParams.get('fecha_ingreso')??'';

        setProducto({
            id:+(id??0), descripcion:descripcion, precio:+precio, fecha_de_ingreso:fecha_ingreso
        })

    },[search, id])


    return(
        <div>
            <h1> Visualizar Producto: {id}</h1>
            <hr/>

            <table className="table table-bordered border-dark-subtle">
                <tbody>
                    <tr>
                        <th className="table-active">ID</th><td>{id}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Descripcion</th><td>{producto?.descripcion}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Precio</th><td>{producto?.precio}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Fecha de Ingreso</th><td>{producto?.fecha_de_ingreso}</td>
                    </tr>
                </tbody>
            </table>
            :<p>Cargando...</p>
            <BotonVolver/>
        </div>
  

     );
    
}