import { BotonVolver } from "../../Components/BotonVolver"
import { useState } from "react"
import { Usuario } from "../../interfaces/Usuario"
import { useForm } from "../../hooks/useForm"
import { useNavigate } from "react-router-dom"


const estadoInicialFormulario={
  username: "", nombre:"", apellido:"", cedula:""
}


export const UsuariosPage = () => {

  const [usuarios, setUsuarios]=useState<Usuario[]>([])


  const {username, nombre, apellido,cedula,onChange,reset,setNewFormValue}=useForm(estadoInicialFormulario)

  const [usuarioSeleccionado, setUsuarioSeleccionado]=useState<number|undefined>(undefined)

  const navigate=useNavigate()

  const eliminarUsuario=(idUsuario:number)=>{
    const nuevaLista=usuarios.filter(usuario=>idUsuario !== usuario.id)
    setUsuarios(nuevaLista)
  }


  const onEditar=()=>{

    if(usuarioSeleccionado){
      const nuevaLista= usuarios.filter(usuario=> usuarioSeleccionado!== usuario.id)
    
     const usuario= {
      id: usuarioSeleccionado,
      username,
      nombre,
      apellido,
      cedula,
    }

    nuevaLista.push(usuario);
    nuevaLista.sort((usu1,usu2)=>usu1.id -usu2.id)

    setUsuarios(nuevaLista)
    
    }
    cerrarModoEdicion()
  }

  const onCancelar=()=>{
    cerrarModoEdicion()
  }

  const cerrarModoEdicion=()=>{
    setUsuarioSeleccionado(undefined)
    reset()
  }

  const abrirModoEdicion=(usuario:Usuario)=>{
    setUsuarioSeleccionado(usuario.id)
    setNewFormValue({
      username:usuario.username,
      nombre:usuario.nombre,
      apellido:usuario.apellido,
      cedula:usuario.cedula
    })
  }

  const onSelect=(usuario:Usuario)=>{

    const params=`?username=${usuario.username}&nombre=${usuario.nombre}&apellido=${usuario.apellido}&cedula=${usuario.cedula}`
    navigate(`/usuarios/${usuario.id}`+params)
 
  }

  const agregarUsuario=(username:string,nombre:string, apellido:string, cedula:string)=>{

    let idMaximo=0;
    usuarios.forEach((usuario)=>{
      if(usuario.id>idMaximo){
        idMaximo=usuario.id
      }

    })

    const nuevoUsuario=
    {
      id:idMaximo+1,
      username,
      nombre,
      apellido,
      cedula
    }

    setUsuarios([...usuarios,nuevoUsuario])
  }


  const onAgregarClicked=()=>{

    if(username.length===0 || apellido.length===0 || nombre.length===0 || cedula.length===0){
      alert("Todos los campos son requeridos")
      return
    }

    agregarUsuario(username, apellido,nombre, cedula)
    reset()
  }


  return (
    <>
            <div className="container mt-5">
              <h2 className="mb-4">Usuarios</h2>
              <hr/>
              <div className="mb-3">
                <label htmlFor="nombre" className="form-label">
                  Nombre
                </label>
                <input className="form-control form-control-xs" type="text" name="nombre" placeholder="Ingrese el Nombre"
                value={nombre} onChange={(event) =>onChange("nombre",event.target.value)}/>
              </div>


              <div className="mb-3">
                <label htmlFor="apellido" className="form-label">
                  Apellido
                </label>
                <input className="form-control form-control-xs" type="text" name="apellido" placeholder="Ingrese el Apellido"
                value={apellido} onChange={(event) =>onChange("apellido",event.target.value)}/>
              </div>


              <div className="mb-3">
                <label htmlFor="cedula" className="form-label">
                  Nº de C.I
                </label>
                <input className="form-control form-control-xs" type="text" name="cedula" placeholder="Ingrese su numero de cedula"
                value={cedula} onChange={(event) =>onChange("cedula",event.target.value)}/>
              </div>


              <div className="mb-3">
                <label htmlFor="username" className="form-label">
                  User Name
                </label>
                <input className="form-control form-control-xs" type="text" name="username" placeholder="Ingrese su Nombre de Usuario"
                value={username} onChange={(event) =>onChange("username",event.target.value)}/>
              </div>
             
              { usuarioSeleccionado?<>
                  <button className="btn btn-primary col-2" onClick={()=> onEditar() } style={{marginLeft:10}}>
                    Editar
                  </button>

                  <button className="btn btn-outline-danger col-2" onClick={()=> onCancelar() } style={{marginLeft:10}}>
                  Cancelar
                </button>
                </>
                :<button className="btn btn-success col-3" onClick={()=> onAgregarClicked() } style={{marginLeft:10}}>
                  Agregar Usuario
                </button>
                }
                
                
                <div className="table-responsive" style={{ marginTop:20, height: '300px', overflowY: 'scroll' }}>
                    <table className="table table-striped table-hover">
                      <thead>
                        <tr className="table-active">
                        <th scope="col" >ID</th>
                          <th className="col-1">Username</th>
                          <th className="col-1">Nombre</th>
                          <th className="col-1">Apellido</th>
                          <th className="col-1">Cedula</th>
                          <th scope="col" >Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      {
                        usuarios.map(usuario=> <tr key={usuario.id} className="user-select-none" style={{cursor:`pointer`}}
                        onClick={()=>onSelect(usuario)}
                        >
                          <th>{usuario.id}</th>
                          <td>{usuario.username}</td>
                          <td>{usuario.nombre}</td>
                          <td>{usuario.apellido}</td>
                          <td>{usuario.cedula}</td>
                          <td>
                            <button className="btn btn-primary"
                              onClick={(event)=>{event.stopPropagation();
                              abrirModoEdicion(usuario)}}>
                                Editar
                            </button>
                          
                            <button className="btn btn-danger"
                              onClick={()=>eliminarUsuario(usuario.id)}>
                                Eliminar
                            </button>
                          </td>
                        </tr>)
                      }
                        
                      </tbody>
                    </table>
                    Total de productos:<b>{usuarios.length}</b>
                  </div>
                  <BotonVolver></BotonVolver>

            </div>        
              
        
    </>
    
  )
  }
