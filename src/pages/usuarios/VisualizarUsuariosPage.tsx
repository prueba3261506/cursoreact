import { useLocation, useParams } from "react-router-dom"
import { Usuario } from "../../interfaces/Usuario"
import { useEffect, useState } from "react"
import { BotonVolver } from "../../Components/BotonVolver"

export const VisualizarUsuariosPage = () => {
    const {id}=useParams()
    const {search}=useLocation()
    const [usuario, setUsuario]=useState<Usuario>()

    useEffect( () => {
        const queryParams=new URLSearchParams(search)
        const username=queryParams.get('username')??'';
        const nombre=queryParams.get('nombre')??'';
        const apellido=queryParams.get('apellido')??'';
        const cedula=queryParams.get('cedula')??'';
        
        
        setUsuario({
            id:+(id??0), username:username, nombre:nombre, apellido:apellido, cedula:cedula
        })

    },[search, id])


    return(
        <div>
            <h1> Visualizar Usuario: {id}</h1>
            <hr/>

            <table className="table table-bordered border-dark-subtle">
                <tbody>
                    <tr>
                        <th className="table-active">ID</th><td>{id}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Username</th><td>{usuario?.username}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Nombre</th><td>{usuario?.nombre}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Apellido</th><td>{usuario?.apellido}</td>
                    </tr>
                    <tr>
                        <th className="table-active">Cedula</th><td>{usuario?.cedula}</td>
                    </tr>
                </tbody>
            </table>
            <BotonVolver/>
        </div>
     );
    
}