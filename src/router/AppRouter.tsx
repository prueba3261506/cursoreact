import { Navigate, Route, Routes } from "react-router-dom"
import { InicioPage } from "../pages/InicioPage"
import { ProductosPage } from "../pages/productos/ProductosPage"
import { UsuariosPage } from "../pages/usuarios/UsuariosPage"
import { CategoriaPage } from "../pages/categorias/CategoriaPage"
import { VisualizarProductosPage } from "../pages/productos/VisualizarProductosPage"
import { VisualizarCategoriasPage } from "../pages/categorias/VisualizarCategoriasPage"
import { VisualizarUsuariosPage } from "../pages/usuarios/VisualizarUsuariosPage"

export const AppRouter= () => {
    return (
        <Routes>

            <Route path="/*" element={<Navigate to='/inicio'/>}/>
            <Route path="inicio" element={<InicioPage/>}/>
            <Route path="productos" element={<ProductosPage/>}/>
            <Route path="productos/:id" element={<VisualizarProductosPage/>}/>
            <Route path="usuarios" element={<UsuariosPage/>}/>
            <Route path="usuarios/:id" element={<VisualizarUsuariosPage/>}/>
            <Route path="Categorias" element={<CategoriaPage/>}/>
            <Route path="Categorias/:id" element={<VisualizarCategoriasPage/>}/>
            
        </Routes>
      
    )
  }
  